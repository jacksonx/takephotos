import pygame
import pygame.camera
import pygame.image
import sys

pygame.init()
pygame.camera.init()

cameras = pygame.camera.list_cameras()

print("Using camera %s ..." % cameras[1])

cam = pygame.camera.Camera(cameras[1])

cam.start()

image = cam.get_image()
pygame.image.save(image, "photos/photo.jpg")